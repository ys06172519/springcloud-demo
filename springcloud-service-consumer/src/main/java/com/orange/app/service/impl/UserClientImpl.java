package com.orange.app.service.impl;

import org.springframework.stereotype.Component;

import com.orange.app.common.model.User;
import com.orange.app.remote.UserClient;
@Component
public class UserClientImpl implements UserClient {

	@Override
	public User getUserInfo(Integer id) {
		User user = new User();
		user.setAge(0);
		user.setId(id);
		user.setUserName("降级");
		return user;
	}

}
