package com.orange.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.orange.app.common.model.User;
import com.orange.app.remote.UserClient;


@RestController
public class FeignTestController {

	@Autowired
	private UserClient userClient;
	//@Autowired
	//private UserService userService;
	
	@RequestMapping(value = "/getUserInfoByFeign", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	User getUserInfo(){
		return userClient.getUserInfo(4);
	}
}
