package com.orange.app.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orange.app.common.model.User;
import com.orange.app.service.impl.UserClientImpl;


@FeignClient(name= "springcloud-service-provider", fallback = UserClientImpl.class )
public interface UserClient {
	
	@RequestMapping(value = "getUserById/{id}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	User  getUserInfo(@PathVariable("id") Integer id);
}
