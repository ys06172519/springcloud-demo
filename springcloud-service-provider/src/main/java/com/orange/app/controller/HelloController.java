package com.orange.app.controller;

import java.util.Random;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.orange.app.common.model.User;


@RestController
public class HelloController {
	
	@RequestMapping(value = "/getUserById/{id}", method = RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public User getUserById(@PathVariable("id") Integer id) throws InterruptedException{
		int sleepTime = new Random().nextInt(3000);
		System.out.println("sleepTime:"+ sleepTime);
		Thread.sleep(sleepTime);
		User user = new User();
		user.setId(1);
		user.setAge(10 + id);
		user.setUserName("张三"+id);
		return user;
	}

}
